﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Linq;

namespace Sotrudnik
{
    class XML
    {
        /// <summary>
        /// Create and write XML to file sotrudxml.xml
        /// </summary>
        /// <param name="sArray">Data Sotrudniki</param>
        public void WriteXML(Sotrudniki[] sArray)
        {
            FileStream fstream = new FileStream("sotrudxml.xml", FileMode.Create);
            XDocument xmldoc = new XDocument(new XDocumentType("Sotrudnik", null, null, null));
            XElement rootxml = new XElement("Sotrudnik");

            XElement xml = new XElement("Sotrudnik", from xmllist in sArray
                                                     select new XElement("Data",
                                                         new XElement("Sotrudfio", xmllist.Name),
                                                         new XElement("Yearwork", xmllist.Year),
                                                         new XElement("Sotrudpost", xmllist.Post),
                                                         new XElement("Sotrudsalary", xmllist.Money),
                                                         new XElement("Experience", xmllist.Stazh)));

            
        //    Console.WriteLine(xml); 
            rootxml.Add(xml);
            xmldoc.Add(rootxml);
            
            xmldoc.Save(fstream);
            fstream.Close();
            Console.WriteLine(Resource.Messages.XMLCreat);
        }

        /// <summary>
        /// Reading XML from a file
        /// </summary>
        /// <param name="filename">filename XML</param>
        public void ReadXML(string filename)
        {
            Console.WriteLine(Resource.Messages.XMLRead);
            XDocument xmldoc = XDocument.Load(filename);
            Console.WriteLine(xmldoc);

            Console.WriteLine(Resource.Messages.XMLReadEnd);                       
        }

    }
}
