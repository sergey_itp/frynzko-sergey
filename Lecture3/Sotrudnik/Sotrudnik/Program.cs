﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace Sotrudnik
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Resource.Messages.DZ3);
            Sotrudniki[] sArray = SotrudData();
            Array.Sort(sArray);
                                  
            XML xml = new XML();
            xml.WriteXML(sArray);
            xml.ReadXML("sotrudxml.xml");           
                       
            Console.ReadLine();
        }

        public static Sotrudniki[] SotrudData()
        {
            Sotrudniki[] sArray = new Sotrudniki[10]
            {
                new Sotrudniki("Петрова А.С.", 2001,"оператор", 300, 15),
                new Sotrudniki("Макаров С.А.", 2005,"кадровик", 400, 10),
                new Sotrudniki("Колесник Я.О.", 2003,"технолог", 450, 11),
                new Sotrudniki("Юрченко А.П.", 2008,"программист", 600, 6),
                new Sotrudniki("Бойко Ю.В.", 2010,"бухгалтер", 450, 5),
                new Sotrudniki("Колотий В.П.", 2007,"маркетолог", 420, 8),
                new Sotrudniki("Безугла Н.О.", 2011,"секретарь", 300, 3),
                new Sotrudniki("Бугаец В.Л.", 2002,"електрик", 400, 15),
                new Sotrudniki("Сидорук А.С.", 2007,"сантехник", 350, 7),
                new Sotrudniki("Сорокин В.Н.", 2004,"плотник", 400, 8),                                
            };
            return sArray;
        }
    }
}
