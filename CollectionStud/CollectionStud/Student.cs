﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CollectionStud
{
    [Serializable]
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Int16 Age { get; set; }
        public string Group { get; set; }
        public Int16 Kurs { get; set; }        
        
        public Student(string firstname, string lastname, Int16 age, string group, Int16 kurs)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Age = age;
            this.Group = group;
            this.Kurs = kurs;  
        }

        public Student()
        {
            // TODO: Complete member initialization
        }

        public override string ToString()
        {
            return string.Format("{0} {1} age={2} group={3} kurs={4}", FirstName, LastName, Age, Group, Kurs);
        }
    }
}
