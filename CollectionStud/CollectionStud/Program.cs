﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CollectionStud
{
    class Program
    {
        static void Main(string[] args)
        {
            Collection group = StudData.GetStudent();
          
            group.StudSavetoXML(@"..\..\studxml.xml");
            group.StudReadXML(@"..\..\studxml.xml");
            group.StudSerializeBinary(@"..\..\stud.dat");
            group.StudDeserializeBinary(@"..\..\stud.dat");
            group.StudSerializeXML(@"..\..\stud.xml");
            group.StudDeserializeXML(@"..\..\stud.xml");
            
                                                       
            Console.ReadLine();
        }


    }
}
