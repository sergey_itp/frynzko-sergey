﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Xml.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace CollectionStud
{
    [Serializable]
    public class Collection : IList<Student>
    {
        private List<Student> _group = new List<Student>();

        #region IList<Student>

        public int IndexOf(Student item)
        {
            return _group.IndexOf(item);
        }

        public void Insert(int index, Student item)
        {
            _group.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _group.RemoveAt(index);
        }

        public Student this[int index]
        {
            get
            {
                return _group[index];
            }
            set
            {
                _group[index] = value;
            }
        }

        #endregion

        #region ICollection<Student>

        public void Add(Student item)
        {
            _group.Add(item);
        }

        public void Clear()
        {
            _group.Clear();
        }

        public bool Contains(Student item)
        {
            return _group.Contains(item);
        }

        public void CopyTo(Student[] array, int arrayIndex)
        {
            _group.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _group.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Student item)
        {
            return _group.Remove(item);
        }

        #endregion

        #region IEnumerable<Student>

        public IEnumerator<Student> GetEnumerator()
        {
            return _group.GetEnumerator();
        }

        #endregion

        #region IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();          
        }

        #endregion


        public void StudSavetoXML(string filename)
        {
            XDocument xmldoc = new XDocument(new XDocumentType("Students", null, null, null));
            XElement rootxml = new XElement("GroupStudent");
            XElement xml = new XElement("Students", from xmllist in StudData.GetStudent()
                                                    select new XElement("DataStudent",
                                                        new XElement("FirstName", xmllist.FirstName),
                                                        new XElement("LastName", xmllist.LastName),
                                                        new XElement("Age", xmllist.Age),
                                                        new XElement("Group", xmllist.Group),
                                                        new XElement("Kurs", xmllist.Kurs)));


            rootxml.Add(xml);
            xmldoc.Add(rootxml);
            xmldoc.Save(filename);
            Console.WriteLine("Create XML file - {0}", filename);
        }

        public void StudReadXML(string filename)
        {
            Console.WriteLine("Students read from XML file - {0}", filename);
            XDocument xmldoc = XDocument.Load(filename);
            Console.WriteLine(xmldoc);
            Console.WriteLine("End read XML file - {0}", filename);
        }

        public void StudSerializeBinary(string filename)
        {
            FileStream fstreamser = new FileStream(filename, FileMode.Create);
            BinaryFormatter serialize = new BinaryFormatter();
            serialize.Serialize(fstreamser, StudData.GetStudent());
            fstreamser.Close();
            Console.WriteLine("\nSerializable Binary Students create file - {0}", filename);
        }
        
        public void StudDeserializeBinary(string filename)
        {
            if (File.Exists(filename))
            {
                Collection grstud = new Collection();
                Console.WriteLine("\nRead deserialize Binary file - {0}", filename);
                FileStream fstreamdes = new FileStream(filename, FileMode.Open);
                BinaryFormatter deserialize = new BinaryFormatter();
                grstud = (Collection)deserialize.Deserialize(fstreamdes);
                foreach (var s in grstud)
                {
                    Console.WriteLine(s);
                }                              
                fstreamdes.Close();
                Console.WriteLine("End deserialize Binary file - {0}", filename);
            }            
        }

        public void StudSerializeXML(string filename)
        {
            FileStream fstreamser = new FileStream(filename, FileMode.Create);
            XmlSerializer serialize = new XmlSerializer(typeof(Collection));
            serialize.Serialize(fstreamser, StudData.GetStudent());
            fstreamser.Close();
            Console.WriteLine("\nSerializable XML Students create file - {0}", filename);
        }

        public void StudDeserializeXML(string filename)
        {
            if (File.Exists(filename))
            {
                Collection grstud = new Collection();
                Console.WriteLine("\nRead deserialize XML file - {0}", filename);
                FileStream fstreamdes = new FileStream(filename, FileMode.Open);
                XmlSerializer deserialize = new XmlSerializer(typeof(Collection));
                grstud = (Collection)deserialize.Deserialize(fstreamdes);
                foreach (var s in grstud)
                {
                    Console.WriteLine(s);
                }
                fstreamdes.Close();
                Console.WriteLine("End deserialize XML file - {0}", filename);
            }
        }

    }
}
           