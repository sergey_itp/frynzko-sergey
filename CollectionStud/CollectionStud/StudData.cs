﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CollectionStud
{
    public class StudData 
    {
        public static Collection GetStudent()
        {
            Collection groupstud = new Collection() 
            { 
                new Student{ FirstName="Ivan", LastName="Petrov", Age=24, Group="ITP", Kurs=3 },
                new Student{ FirstName="Petr", LastName="Makarov", Age=27, Group="SUA", Kurs=5 },
                new Student{ FirstName="Sergey", LastName="Melnik", Age=20, Group="SUA", Kurs=2 },
                new Student{ FirstName="Alex", LastName="Matveev", Age=19, Group="ITP", Kurs=1 },
                new Student{ FirstName="Roman", LastName="Viter", Age=21, Group="SUA", Kurs=1 },
                new Student{ FirstName="Anna", LastName="Berner", Age=26, Group="ITP", Kurs=3 },
                new Student{ FirstName="Dima", LastName="Danilov", Age=21, Group="ITP", Kurs=2 },
                new Student{ FirstName="Ivan", LastName="Voronin", Age=20, Group="ITP", Kurs=3 }

            };
            return groupstud;               
           
        }
        
    }
}
