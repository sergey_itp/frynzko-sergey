﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Bank
{
    public class VkladCollection : IList<Vklad>, IEnumerable
    {

        private List<Vklad> investors = new List<Vklad>();

        public int IndexOf(Vklad item)
        {
            return investors.IndexOf(item);
        }

        public void Insert(int index, Vklad item)
        {
            investors.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            investors.RemoveAt(index);
        }

        public Vklad this[int index]
        {
            get
            {
                return investors[index];
            }
            set
            {
                investors[index] = value;
            }
        }

        public void Add(Vklad item)
        {
            investors.Add(item);
        }

        public void Clear()
        {
            investors.Clear();
        }

        public bool Contains(Vklad item)
        {
            return investors.Contains(item);
        }

        public void CopyTo(Vklad[] array, int arrayIndex)
        {
            investors.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return investors.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Vklad item)
        {
            return investors.Remove(item);
        }

        public IEnumerator<Vklad> GetEnumerator()
        {
            return investors.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return investors.GetEnumerator();
        }

        public void Sort()
        {
            investors.Sort();
        }
      
     }
}
