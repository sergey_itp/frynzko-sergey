﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bank
{
    public class Vklad   : IComparable<Vklad>
    {
        public Vklad(string name, Int64 acct, double sum, int year)
        {
            this.Name = name;
            this.Acct = acct;
            this.Sum = sum;
            this.Year = year;    
        }

        public Vklad()
        {
            // TODO: Complete member initialization
        }

        public string Name { get; set; }
        public Int64 Acct { get; set; }
        public double Sum { get; set; }
        public int Year { get; set; }

        
        public override string ToString()
        {
            return String.Format("{0}" + ", счет №: " + "{1}" + ", вклад: " + "{2:F2}" + "$ , положил в " + "{3}" + " г.", Name, Acct, Sum, Year);
        }
                 

        public int CompareTo(Vklad x)
        {
            if (this.Sum > x.Sum) return 1;
            if (this.Sum < x.Sum) return -1;
            return 0;
        }
    }
}
