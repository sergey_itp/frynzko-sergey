﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Bank
{
    public class VkladEnum : IEnumerable, IEnumerator
    {
        public Vklad[] _vkladchiki;
        int position = -1;

        public VkladEnum (Vklad[] vArray)
        {
            _vkladchiki = new Vklad[vArray.Length];
            for (var i = 0; i < vArray.Length; i++)
            {
                _vkladchiki[i] = vArray[i];            
            }      
        }

        public bool MoveNext()
        { 
            position++;
            return (position < _vkladchiki.Length);       
        }

        public void Reset()
        {
            position = -1; 
        }

        object IEnumerator.Current
        {
            get
            { return Current; }   
        }

        public Vklad Current
        {
            get
            {
                try
                {
                    return _vkladchiki[position];                
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();  
                }          
            }  
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return  (IEnumerator)GetEnumerator();
        }

        public VkladEnum GetEnumerator()
        {
            return new VkladEnum(_vkladchiki);
        }
     }
}
