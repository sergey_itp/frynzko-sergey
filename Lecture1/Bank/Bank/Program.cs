﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Bank
{
    class Program
    {
        static void Main(string[] args)
        {
            VkladCollection collection = new VkladCollection();

            GetInvestors(collection);

            Console.WriteLine(Resource.Messages.DZ1_6);
            DateTime dat = DateTime.Now;            
            int dt = Convert.ToInt16(dat.Year);

            Console.WriteLine("\n{0} {1}\n", Resource.Messages.DateYear, dat.Year);

            collection.Sort();

            foreach (Vklad v in collection)
            {
                if (dt == v.Year)
                {
                    Console.WriteLine(String.Format("{0}" + ", счет №: " + "{1}" + ", вклад: " + "{2:F2}" + "$ , положил в " + "{3}" + " г.", v.Name, v.Acct, v.Sum, v.Year));
                }
            }                
                      
            Console.ReadLine();  
        }


        public static void GetInvestors(VkladCollection collection)
        {
           
                collection.Add(new Vklad("Петрова А.С.", 26002155684, 3000, 2010));
                collection.Add(new Vklad("Макаров С.А.", 26006545684, 3800, 2010));
                collection.Add(new Vklad("Колесник Я.О.", 26001255684,3500, 2010));
                collection.Add(new Vklad("Юрченко А.П.", 26002875684, 10000, 2013));
                collection.Add(new Vklad("Бойко Ю.В.", 26002151854, 4500, 2012));
                collection.Add(new Vklad("Колотий В.П.", 26006515684, 4200, 2012));
                collection.Add(new Vklad("Безугла Н.О.", 26009865684, 6000, 2013));
                collection.Add(new Vklad("Бугаец В.Л.", 26005565684, 4000, 2013));
                collection.Add(new Vklad("Сидорук А.С.", 26002465684, 3500, 2012));
                collection.Add(new Vklad("Сорокин В.Н.", 26003665684, 8000, 2013));           
           
        }

    }
}
