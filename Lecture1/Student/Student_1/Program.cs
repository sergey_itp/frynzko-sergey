﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Student
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentCollection collection = new StudentCollection();

            GetStudents(collection);

            Console.WriteLine(Messages.Messages.DZ1_1);

            foreach (var studall in collection)
            {                
                Console.WriteLine(studall);
            }

            Console.Write("\n{0} ", Messages.Messages.NumberSchool);
            string str = Console.ReadLine();

            collection.Sort();

            foreach (var stud in collection)
            {               
                if (stud.School == str)
                {                    
                    Console.WriteLine(stud);
                }
            }
            
            Console.ReadLine();
        }



        private static void GetStudents(StudentCollection collection)
        {           
               collection.Add(new Student("Петров","Анатолий","Сергеевич",1986,"ул.Мира 13","31"));
               collection.Add(new Student("Макаров","Сергей","Анатолиевич",1985,"ул.Первомайская 14","6"));
               collection.Add(new Student("Колесник","Яна","Олеговна",1987,"ул.Карнаухова 20","17"));
               collection.Add(new Student("Юрченко","Александр","Павлович",1988,"ул.Тельмана 15","11"));
               collection.Add(new Student("Бойко","Юрий","Викторович",1985,"пер.Тергенева 13","11"));
               collection.Add(new Student("Колотий","Василий","Павлович",1980,"ул.Октябрьская 23","11"));
               collection.Add(new Student("Сидорук","Александ","Сергеевич",1983,"ул.Щорса 98","31"));
               collection.Add(new Student("Бугаец","Виктор","Павлович",1985,"ул.Киевская 27","6"));
               collection.Add(new Student("Безугла","Наталия","Петровна",1987,"ул.Котляревского 3а","6"));
               collection.Add(new Student("Сорокин","Валентин","Николаевич",1987,"ул.Ленина 66","31"));                           
        }     
    }
}
