﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Student
{
    public class Student : IComparable<Student>
    {
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string Othername { get; set; }
        public int Birthday { get; set; }
        public string Adress { get; set; }
        public string School { get; set; }
        
        
        public Student(string firstname, string lastname, string othername, int birthday, string adress, string school)
        {
            this.FirstName = firstname;
            this.Lastname = lastname;
            this.Othername = othername;
            this.Birthday = birthday;
            this.Adress = adress;
            this.School = school;
        }

        public Student()
        {
            // TODO: Complete member initialization
        }          
        

        public override string  ToString()
        {
            return string.Format(FirstName + " " + Lastname + " " + Othername + ", год рождения: [" + Birthday + "], " + Adress + ", школа №" + School);
        }


        public int CompareTo(Student x)
        {
            if (this.Birthday > x.Birthday) return 1;
            if (this.Birthday < x.Birthday) return -1;
            return 0;
        }
    }
}
