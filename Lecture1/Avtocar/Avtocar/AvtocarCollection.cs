﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Avtocar
{
    public class AvtocarCollection : IList<Avtocar>, IEnumerable
    {

        private List<Avtocar> cars = new List<Avtocar>();

        public int IndexOf(Avtocar item)
        {
            return cars.IndexOf(item);
        }

        public void Insert(int index, Avtocar item)
        {
            cars.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            cars.RemoveAt(index);
        }

        public Avtocar this[int index]
        {
            get
            {
                return cars[index];
            }
            set
            {
                cars[index] = value;
            }
        }

        public void Add(Avtocar item)
        {
            cars.Add(item);
        }

        public void Clear()
        {
            cars.Clear();
        }

        public bool Contains(Avtocar item)
        {
            return cars.Contains(item);
        }

        public void CopyTo(Avtocar[] array, int arrayIndex)
        {
            cars.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return cars.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Avtocar item)
        {
            return cars.Remove(item);
        }

        public IEnumerator<Avtocar> GetEnumerator()
        {
            return cars.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return cars.GetEnumerator();
        }

        public void Sort()
        {
            cars.Sort();        
        }

    }
}
