﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avtocar
{
    class Program
    {
        static void Main(string[] args)
        {
            AvtocarCollection collection = new AvtocarCollection();
            
            GetCars(collection);

            Console.WriteLine(Message.Messages.DZ1_4);

            foreach (Avtocar carall in collection)
            {
                Console.WriteLine(carall);                
            }

            collection.Sort();

            Console.Write("\n{0}", Message.Messages.EnterYear);           

            try
            {
                int god = Convert.ToInt16(Console.ReadLine());

                foreach (Avtocar car in collection)
                {
                    if (god > car.Yearcar)
                    {
                        Console.WriteLine(car);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();    

        }

        private static void GetCars(AvtocarCollection collection)
        {
                collection.Add(new Avtocar("Opel Astra","ВН 1236 АА","Фринцко", 2008, 63000));
                collection.Add(new Avtocar("Volkswagen Passat","ОР 5296 АА","Ляпун", 1997, 97000));
                collection.Add(new Avtocar("Peugeot 807","ВН 1889 АО","Сидоркин", 1994, 161000));
                collection.Add(new Avtocar("ВАЗ 2107","ВЫ 5389 АО","Юрченко", 1983, 681000));
                collection.Add(new Avtocar("Opel Vectra","ВІ 2598 ОА","Петров", 1998, 157000));
                collection.Add(new Avtocar("Daewoo  Lanos","ПП 1298 АО","Малькова", 1995, 187000));
                collection.Add(new Avtocar("Honda Accord","НО 5388 ОО","Сидорук", 1998, 213000));
                collection.Add(new Avtocar("Volkswagen Golf","ПО 4152 ОО","Кротюк", 2007, 126000));
                collection.Add(new Avtocar("Lexus GS 250","НН 5523 АА","Антонов", 2008, 112000));
                collection.Add(new Avtocar("Porsche Cayenne", "ОО 1111 АА", "Мельник", 2010, 56000));           
        }  
    }
}
