﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Avtocar
{
    public class Avtocar : IComparable<Avtocar>
    {
        public string Markacar { get; set; }
        public string Numbercar { get; set; }
        public string Name { get; set; }
        public int Yearcar { get; set; }
        public Int32 Probeg { get; set; }
        
        public Avtocar(string markacar, string numbercar, string name, int yearcar, Int32 probeg)
        {
            this.Markacar = markacar;
            this.Numbercar = numbercar;
            this.Name = name;
            this.Yearcar = yearcar;
            this.Probeg = probeg;        
        }

        public Avtocar()
        {
            // TODO: Complete member initialization
        }


        public override string ToString()
        {
            return string.Format("{0}" + " № " + "{1}" + ", влад-ц: " + "{2}" + " куплена:[" + "{3}" + "], " + "пробег: " 
                                                                   + "{4}" + " км.", Markacar, Numbercar, Name, Yearcar, Probeg);
        }

        public int CompareTo(Avtocar x)
        {
            if (this.Probeg > x.Probeg) return 1;
            if (this.Probeg < x.Probeg) return -1;
            return 0;
        }
    }
}
