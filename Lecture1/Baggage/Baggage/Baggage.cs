﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Baggage
{
    public class Baggage : IComparable<Baggage>
    {
        public string Firstname;
        public string Lastname;
        public int Kolbaggage;
        public double Vesbaggage;

        public Baggage(string firstname, string lastname, int kolbaggage, double vesbaggage)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Kolbaggage = kolbaggage;
            this.Vesbaggage = vesbaggage;
        }

        public Baggage()
        {
            // TODO: Complete member initialization
        }

        public override string ToString()
        {
            return string.Format("{0}" + " " + "{1}" + " " + ", кол.вещей: [" + "{2}" + "], " + "общий вес: " +
                                                    "{3:F3}" + " кг.", Firstname, Lastname, Kolbaggage, Vesbaggage);
        }
        


        public int CompareTo(Baggage x)
        {
            if (this.Kolbaggage > x.Kolbaggage) return 1;
            if (this.Kolbaggage < x.Kolbaggage) return -1;
            return 0;
        }
    }
}
