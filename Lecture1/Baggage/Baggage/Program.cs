﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Baggage
{
    class Program
    {
        public static void Main(string[] args)
        {
            BaggageCollection collection = new BaggageCollection();

            GetBaggage(collection);

            Console.WriteLine(Messages.Messages.DZ1_3);

            foreach (var bagall in collection)
            {
                Console.WriteLine(bagall);
            }

                        
            try
            {
                collection.Sort();

                Console.Write("\n{0} ", Messages.Messages.InputWeight);
                
                double ves = Convert.ToDouble(Console.ReadLine());

                Console.Write("{0}\n", Messages.Messages.BaggageWeight);
    
                foreach (Baggage b in collection)
                {
                    if ((b.Vesbaggage / b.Kolbaggage) >= ves)
                    {
                        Console.WriteLine(b + " ср.вес: " + "{0:F3}" + " кг.", (b.Vesbaggage / b.Kolbaggage));                      
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(Messages.Messages.Error);
            }       

                        
            Console.ReadLine();
        }

        private static void GetBaggage(BaggageCollection collection)
        {
               collection.Add(new Baggage("Петров","Анатолий", 5, 12.800));
               collection.Add(new Baggage("Макаров", "Сергей", 3, 4.100));
               collection.Add(new Baggage("Колесник", "Яна", 2, 6.300));
               collection.Add(new Baggage("Юрченко", "Юрий", 3, 7.855));
               collection.Add(new Baggage("Бойко", "Юрий", 1, 2.600));
               collection.Add(new Baggage("Колотий", "Василий", 3, 8.800));
               collection.Add(new Baggage("Сидорук", "Александ", 5, 15.250));
               collection.Add(new Baggage("Бугаец", "Виктор", 1, 3.700));
       
        }
    }
}
