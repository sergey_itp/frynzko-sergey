﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Baggage
{
    class BaggageCollection : IList<Baggage>, IEnumerable
    {

        private List<Baggage> bags = new List<Baggage>();

        public int IndexOf(Baggage item)
        {
            return bags.IndexOf(item);
        }

        public void Insert(int index, Baggage item)
        {
            bags.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            bags.RemoveAt(index);
        }

        public Baggage this[int index]
        {
            get
            {
                return bags[index];
            }
            set
            {
                bags[index] = value;
            }
        }

        public void Add(Baggage item)
        {
            bags.Add(item);
        }

        public void Clear()
        {
            bags.Clear();
        }

        public bool Contains(Baggage item)
        {
            return bags.Contains(item);
        }

        public void CopyTo(Baggage[] array, int arrayIndex)
        {
            bags.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return bags.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Baggage item)
        {
            return bags.Remove(item);
        }

        public IEnumerator<Baggage> GetEnumerator()
        {
            return bags.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return bags.GetEnumerator();
        }

        public void Sort()
        {
            bags.Sort();
        }
    }
}
