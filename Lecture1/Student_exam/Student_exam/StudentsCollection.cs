﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Student_exam
{
    class StudentsCollection : IList<Student>, IEnumerable
    {

        private List<Student> stud = new List<Student>();

        public int IndexOf(Student item)
        {
            return stud.IndexOf(item);
        }

        public void Insert(int index, Student item)
        {
            stud.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            stud.RemoveAt(index);
        }

        public Student this[int index]
        {
            get
            {
                return stud[index];
            }
            set
            {
                stud[index] = value;
            }
        }

        public void Add(Student item)
        {
            stud.Add(item);
        }

        public void Clear()
        {
            stud.Clear();
        }

        public bool Contains(Student item)
        {
            return stud.Contains(item);
        }

        public void CopyTo(Student[] array, int arrayIndex)
        {
            stud.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return stud.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Student item)
        {
            return stud.Remove(item);
        }

        public IEnumerator<Student> GetEnumerator()
        {
            return stud.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return stud.GetEnumerator();
        }

        public void Sort()
        {
            stud.Sort();
        }
    }
}
