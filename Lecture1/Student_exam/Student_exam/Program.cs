﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Student_exam
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentsCollection collection = new StudentsCollection();
            
            GetStudents(collection);

            Console.WriteLine(Resource.Messages.DZ1_2);

            Console.WriteLine("{0}[{1}]", Resource.Messages.AllStudent, collection.Count());
            foreach (var sall in collection)
            {
                Console.WriteLine(sall);
            }

            Console.WriteLine("\n{0}", Resource.Messages.Student);

            collection.Sort();

            foreach (var s in collection)
            {
                if ((s.Exam1 >= 7) && (s.Exam2 >= 7) && (s.Exam3 >= 7))
                {
                    Console.WriteLine(s);
                }
            }
                       
            Console.ReadLine();            
        }

        private static void GetStudents(StudentsCollection collection)
        {           
               collection.Add(new Student("Петров","Анатолий", 11, 10, 12, 11));
               collection.Add(new Student("Макаров","Сергей", 21, 8, 7, 8));
               collection.Add(new Student("Колесник","Яна", 11, 10, 9, 7));
               collection.Add(new Student("Юрченко","Александр", 11, 4, 5, 3));
               collection.Add(new Student("Бойко","Юрий", 31, 6, 5, 3));
               collection.Add(new Student("Колотий","Василий", 21, 9, 10, 10));
               collection.Add(new Student("Сидорук","Александ", 11, 4, 3, 4));
               collection.Add(new Student("Бугаец","Виктор", 21, 6, 4, 4));
               collection.Add(new Student("Безугла","Наталия", 31, 8, 7, 7));
               collection.Add(new Student("Сорокин", "Валентин", 31, 8, 7, 10));               
           
        }
              
    }
}
