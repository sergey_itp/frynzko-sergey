﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Student_exam
{
    public class Student : IComparable<Student>
    {

        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int Group { get; set; }
        public int Exam1 { get; set; }
        public int Exam2 { get; set; }
        public int Exam3 { get; set; }
        
        public Student (string firstname, string lastname, int group, int exam1, int exam2, int exam3)
        {
            this.Firstname = firstname;
            this.Lastname = lastname;
            this.Group = group;
            this.Exam1 = exam1;
            this.Exam2 = exam2;
            this.Exam3 = exam3;
        }

        public Student()
        {
            // TODO: Complete member initialization
        }
 

        public override string  ToString()
        {
            return string.Format(Firstname + " " + Lastname + " " + ", группа ИТП № " + Group 
                                           + ", оценки по экзаменам: " + Exam1 + ", " + Exam2 + ", " + Exam3);
        }
        
        
        public int CompareTo(Student x)
        {
            if (this.Group > x.Group) return 1;
            if (this.Group < x.Group) return -1;
            return 0;
        }
    }
}
