﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Sotrudnik
{
    public class SotrudnikCollection : IList<Sotrudniki>, IEnumerable
    {

        private List<Sotrudniki> staff = new List<Sotrudniki>();

        public int IndexOf(Sotrudniki item)
        {
            return staff.IndexOf(item);
        }

        public void Insert(int index, Sotrudniki item)
        {
            staff.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            staff.RemoveAt(index);
        }

        public Sotrudniki this[int index]
        {
            get
            {
                return staff[index];
            }
            set
            {
                staff[index] = value;
            }
        }

        public void Add(Sotrudniki item)
        {
            staff.Add(item);
        }

        public void Clear()
        {
            staff.Clear();
        }

        public bool Contains(Sotrudniki item)
        {
            return staff.Contains(item);
        }

        public void CopyTo(Sotrudniki[] array, int arrayIndex)
        {
            staff.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return staff.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(Sotrudniki item)
        {
            return staff.Remove(item);
        }

        public IEnumerator<Sotrudniki> GetEnumerator()
        {
            return staff.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return staff.GetEnumerator();
        }

        public void Sort()
        {
            staff.Sort();
        }
    }
}
