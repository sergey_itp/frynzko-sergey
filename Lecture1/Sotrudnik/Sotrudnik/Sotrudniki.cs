﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sotrudnik
{
    public class Sotrudniki : IComparable<Sotrudniki>
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public string Post { get; set; }
        public int Money { get; set; }
        public int Stazh { get; set; }        
        
        public Sotrudniki(string name, int year, string post, int money, int stazh)
        {
            this.Name = name;
            this.Year = year;
            this.Post = post;
            this.Money = money;
            this.Stazh = stazh;        
        }

        public Sotrudniki()
        {
            // TODO: Complete member initialization
        }


        public override string ToString()
        {
            return string.Format("{0}" + " принят: " + "{1}" + ", должность: " + "{2}" + " з/п: " + "{3}" + 
                                                                                       " $, стаж: " + "{4}" + " л.", Name, Year, Post, Money, Stazh);
        }
                

        public int CompareTo(Sotrudniki x)
        {
            if (this.Stazh > x.Stazh) return 1;
            if (this.Stazh < x.Stazh) return -1;
            return 0;
        }
    }
}
