﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace Sotrudnik
{
    public class SotrudnikEnum : IEnumerable, IEnumerator
    {
            public Sotrudniki[] _sotrudniki;
            int position = -1;

            public SotrudnikEnum(Sotrudniki[] sArray)
            {
                _sotrudniki = new Sotrudniki[sArray.Length];
                for (int i = 0; i < sArray.Length; i++)
                {
                    _sotrudniki[i] = sArray[i];
                }

            }

            public bool MoveNext()
            {
                position++;
                return (position < _sotrudniki.Length);
            }

            public void Reset()
            { 
                position = -1; 
            }

            object IEnumerator.Current
            {
                get
                { return Current; }
            }

            public Sotrudniki Current
            {
                get
                {
                    try
                    {
                        return _sotrudniki[position];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new InvalidOperationException();
                    }
                }
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return (IEnumerator)GetEnumerator();
            }

            public SotrudnikEnum GetEnumerator()
            {
                return new SotrudnikEnum(_sotrudniki);
            }
    }
}
