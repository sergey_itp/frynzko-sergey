﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sotrudnik
{
    class Program
    {
        static void Main(string[] args)
        {
            SotrudnikCollection collection = new SotrudnikCollection();
           
            GetSotrudniki(collection);

            Console.WriteLine(Resource.Messages.DZ1_5);

            foreach (Sotrudniki sotrudall in collection)
            {
                Console.WriteLine(sotrudall);
            }

            collection.Sort();

            try
            {
                Console.Write("\n {0}", Resource.Messages.EnterMoney);
                int money = Convert.ToInt16(Console.ReadLine());

                foreach (Sotrudniki s in collection)
                {
                    if (money > s.Money)
                    {
                        Console.WriteLine(s);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
                       
            Console.ReadLine();
        }


        public static void GetSotrudniki(SotrudnikCollection collection)
        {          
                collection.Add(new Sotrudniki("Петрова А.С.", 2001,"оператор", 300, 15));
                collection.Add(new Sotrudniki("Макаров С.А.", 2005,"кадровик", 400, 10));
                collection.Add(new Sotrudniki("Колесник Я.О.", 2003,"технолог", 450, 11));
                collection.Add(new Sotrudniki("Юрченко А.П.", 2008,"программист", 600, 6));
                collection.Add(new Sotrudniki("Бойко Ю.В.", 2010,"бухгалтер", 450, 5));
                collection.Add(new Sotrudniki("Колотий В.П.", 2007,"маркетолог", 420, 8));
                collection.Add(new Sotrudniki("Безугла Н.О.", 2011,"секретарь", 300, 3));
                collection.Add(new Sotrudniki("Бугаец В.Л.", 2002,"електрик", 400, 15));
                collection.Add(new Sotrudniki("Сидорук А.С.", 2007,"сантехник", 350, 7));
                collection.Add(new Sotrudniki("Сорокин В.Н.", 2004, "плотник", 400, 8));
        }
    }
}
