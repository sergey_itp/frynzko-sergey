﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Delegate
{       
    public class MyFunction
    {
        public delegate double MyMath(double val);
 
        public static void FuncPrint(MyMath fsin, MyMath fcos, MyMath ftan, double xn, double xk, double dx)
        {
            Console.WriteLine("|  Sin   |  Cos   |  Tan   |");
            for (double i = xn; i < xk; i += dx)
            {
                Console.WriteLine("| {0:F4} | {1:F4} | {2:F4} |", fsin(i), fcos(i), ftan(i));
            }
        }

        public static void Print(object obj)
        {
             Console.WriteLine("\nDateTime event: {0}", DateTime.Now.ToString("hh:mm:ss"));
             FuncPrint(Math.Sin, Math.Cos, Math.Tan, 0, 1.5, 0.2);
        }

    }
}
