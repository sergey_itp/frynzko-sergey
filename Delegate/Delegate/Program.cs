﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Delegate
{
    class Program
    {   

        static void Main(string[] args)
        {
            Console.WriteLine("Create timer.");
            TimerCallback callback = new TimerCallback(MyFunction.Print);
            Timer stateTimer = new Timer(callback, null, 0, 3000);   
            
                                             
            Console.ReadLine();
        } 
       
    }
}
