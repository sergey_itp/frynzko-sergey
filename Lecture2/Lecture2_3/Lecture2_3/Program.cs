﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Lecture2_3
{
    class Program
    {
        static void Main(string[] args)
        {

            string str, strraz, found;
            Console.WriteLine(Resource.Messages.DZ2_3);
            Console.Write("{0} ", Resource.Messages.InputName);
            str = Console.ReadLine();                      

            int i = 0;
            found = "";
            strraz = @"(^|\s?)[b,c,d,f,g,h,j,k,l,m,n,p,q,r,s,t,v,w,x,z]{2,4}[\w]*($|\s?)";
            Regex reg = new Regex(strraz);
            Match match = reg.Match(str);
            while (match.Success)
            {
                found = match.Value;
                Console.WriteLine("{0} {1}", Resource.Messages.Find, found);
                match = match.NextMatch();
                i++;
            }

            Console.WriteLine("{0} {1}", Resource.Messages.FindCounter, i);
            Console.ReadLine();
        }
    }
}
