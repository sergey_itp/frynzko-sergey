﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Lecture2_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("HOMEWORK № 2_8.");
            Console.Write("Input string: ");
            string str = Console.ReadLine();
            string str1, str2;
            int position = str.IndexOf('>');
            string maska = @"\.";
           
            if (position == -1)
            {
                str1 = str;
                str2 = "";
            }
            else
            {
                int length = str.Length;
                str1 = str.Substring(0, position);
                str2 = str.Substring(position, length - position);
            }
            Regex reg = new Regex(maska);
            Match match = reg.Match(str1);
            while (match.Success)
            {
                str1 = Regex.Replace(str1, maska, "<br/>");
                match = match.NextMatch();
            }
            Console.WriteLine("Result: {0}{1}", str1, str2);
            Console.ReadLine();
        }

        
    }
}
