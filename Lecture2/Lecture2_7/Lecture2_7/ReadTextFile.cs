﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace Lecture2_7
{
    class ReadTextFile
    {
        public void text()
        { 
            var str = File.ReadAllText("Text.txt");
            string maska_a, maska_b, maska_c, maska_d, maska_e, maska_f, maska_url;
            maska_a = @"^[\w]+[\s]";
            maska_b = @"[\s][\w]+($|\.|\r)";
            maska_c = @"^[\s]+[\w]+[\s]";
            maska_d = @"^[\s]+.+($|\r)";
            maska_e = @"^";
            maska_f = @".+($|\r)";
            maska_url = @"http://[\w.\w/]+";
           
            Console.WriteLine("a)");
            reg_abc(maska_a, str);
            Console.WriteLine("b)");
            reg_abc(maska_b, str);
            Console.WriteLine("c)");
            reg_abc(maska_c, str);
            Console.WriteLine("d)");
            Match match_d = reg_def(maska_d, str);
            while (match_d.Success)
            {
                Console.WriteLine("  {0}", match_d.Value);
                match_d = match_d.NextMatch();
            }
            Console.WriteLine("e)");
            Match match_e = reg_def(maska_e, str);
            while (match_e.Success)
            {
                Console.WriteLine(" {0}", match_e.Index);
                match_e = match_e.NextMatch();
            }
            Console.WriteLine("f)");
            int i = 1;
            Match match_f = reg_def(maska_f, str);
            while (match_f.Success)
            {
               
                Match match = reg_def(maska_url, match_f.Value);
                while (match.Success)
                {
                    Console.WriteLine("  URL: {0} Line: {1} Position: {2}", match.Value, i, match.Index);
                    match = match.NextMatch();
                }
                i++;
                match_f = match_f.NextMatch();
            }

        
        }

        public void reg_abc(string maska, string str)
        {            
            Regex reg = new Regex(maska, RegexOptions.Multiline);
            Match match = reg.Match(str);
            while (match.Success)
            {
                Console.WriteLine(" {0}",match.Value);
                match = match.NextMatch();
            }
        }

        public Match reg_def(string maska, string str)
        {
            Regex reg = new Regex(maska, RegexOptions.Multiline);
            Match match = reg.Match(str);
            return match;
        }    
    }

}
