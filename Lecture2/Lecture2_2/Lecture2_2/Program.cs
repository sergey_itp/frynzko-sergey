﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Lecture2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string str, strraz, found;

            Console.WriteLine(Resource.Messages.DZ2_2);
            Console.Write(Resource.Messages.InputName);
            str = Console.ReadLine();

            strraz = @"(\s?|^)a[\w]+a(\s?|$)";
            Regex raz = new Regex(strraz);
            
            Match match = raz.Match(str);
            found = "";
            Console.WriteLine("\n{0}", Resource.Messages.Maska);
            
            int i = 0;
            while (match.Success)
            {
                found = match.Value;
                Console.WriteLine("{0} {1}", Resource.Messages.Find, found);
                match = match.NextMatch();
                i++;
            }
            Console.WriteLine("{0} {1}", Resource.Messages.FindCounter, i);
            Console.ReadLine();
        }   
    }
}
