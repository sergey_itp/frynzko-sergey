﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Lecture2_5
{
    class Program
    {
        static void Main(string[] args)
        {
            string str, strraz, found;
            Console.WriteLine(Resource.Messages.DZ2_5);
            Console.WriteLine("{0}", Resource.Messages.Mask);
            Console.Write("{0} ", Resource.Messages.InputName);
            str = Console.ReadLine();
            strraz = @"[\][\w]+[.][\w]{3,7}";            
            Regex raz = new Regex(strraz);
            Match match = raz.Match(str);
            found = "";

            while (match.Success)
            {
                found = match.Value;
                Console.WriteLine("{0} {1}", Resource.Messages.Find, found);
                match = match.NextMatch();
            }          
            Console.ReadLine();
        }
    }
}
