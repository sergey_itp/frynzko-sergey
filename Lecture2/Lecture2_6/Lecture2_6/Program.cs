﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Lecture2_6
{
    class Program
    {
        static void Main(string[] args)
        {
            string str, strraz, found;
            Console.WriteLine(Resource.Messages.DZ2_6);
            str = "http://edu.mmcs.sfedu.ru/course/view.php?id=9";
            Console.WriteLine("{0} {1}", Resource.Messages.Input, str);
            strraz = @"http://[\w.\w/]+[?][\w=\d]+";
            
            Regex raz = new Regex(strraz);
            Match match = raz.Match(str);
            found = "";
                       
            while (match.Success)
            {
                found = match.Value;
                Console.WriteLine("{0} {1}", Resource.Messages.Find, found);
                match = match.NextMatch();
            }              
            Console.ReadLine();            
        }
    }
}
