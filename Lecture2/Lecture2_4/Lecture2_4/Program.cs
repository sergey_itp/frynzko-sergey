﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Lecture2_4
{
    class Program
    {
        static void Main(string[] args)
        {
            string str, strraz, found;
            Console.WriteLine(Resource.Messages.DZ2_4);
            Console.Write("{0} ", Resource.Messages.InputName);
            str = Console.ReadLine();
            strraz = @"[\d]+(,|.)[\d]+";

            Regex raz = new Regex(strraz);
            Match match = raz.Match(str);
            found = "";
           
            int i = 0;
            decimal s = 0;
            while (match.Success)
            {
                found = match.Value;
                Console.WriteLine("{0} {1}", Resource.Messages.Find, found);
                s += Convert.ToDecimal(found);
                match = match.NextMatch();
                i++;
            }

            Console.WriteLine("{0} {1}", Resource.Messages.Summa, s);
            Console.ReadLine();
        }
    }
}
